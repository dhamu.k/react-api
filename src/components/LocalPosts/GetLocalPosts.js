import React, { Component } from 'react';

import posts from './posts.js';
class GetLocalPosts extends Component {
    constructor(props){
        super(props);
        this.state = {            
            posts :posts            
        };
    }

    render() {
        const {posts} = this.state;
        return(
            <div>
                <ol className="item">
                {
                    posts.map(post => (
                        <li >
                            <div>
                                <table border="1">
                                    <tr>
                                   <td> <th>Student Id</th></td>
                                   <td><th>Student Name</th></td>
                                   <td><th>Department</th></td>
                                    </tr>
                                    <tr>
                                        <td><p>{post.id}</p></td>
                                        <td><p>{post.name}</p></td>
                                        <td><p>{post.dept}</p></td>
                                    </tr>
                                </table>
                            </div>
                        </li>
                    ))
                }
                </ol>
            </div>
        );
    }
  }
  
  export default GetLocalPosts;